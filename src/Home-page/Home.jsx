import styles from "./home.module.css"

export function Home() {
    return (
        <>
        <div className={styles.fundo}>
            <section className={styles.imagem}>
                <img src="src/assets/Paisagem1.png" alt=" "></img>
            </section>
            <section className={styles.boas_vindas}>
                <div>
                    <h1>Boas-vindas!</h1>
                    <div className={styles.barra}></div>
                </div>
                <div>
                    <p>Somos uma agência apaixonada por criar viagens únicas e inesquecíveis para nossos clientes. Com anos de experiência no setor de turismo, nossa equipe está mais do que preparada para ajudar você a planejar a viagem dos seus sonhos.</p>
                    <p>Da escolha do destino à seleção de acomodações, atividades e transporte, cuidamos de todos os detalhes para que você aproveite ao máximo cada momento da sua jornada!</p>
                </div>
            </section>
            <section className={styles.ofertas}>
                <div>
                    <h1>Ofertas da semana</h1>
                    <div className={styles.barra}></div>
                </div>
                <div>
                    <div className={styles.linha1}>
                        <div className={styles.japao}>
                            <div>
                                <p>Hotel+Aéreo</p>
                                <h1>Japão</h1>
                            </div>
                            <h2>R$ 4.000</h2>
                            <button>Ver detalhes</button>
                        </div>
                        <div className={styles.sanandres}>
                            <div>
                                <p>Hotel+Aéreo</p>
                                <h1>San Andrés</h1>                               
                            </div>
                            <h2>R$ 3.000</h2>
                            <button>Ver detalhes</button>
                        </div>
                    </div>
                    <div className={styles.linha1}>
                        <div className={styles.para}>
                            <div>
                                <p>Hotel+Aéreo</p>
                                <h1>Paraíba</h1>
                            </div>
                            <h2>R$ 1.200</h2>
                            <button>Ver detalhes</button>
                        </div>
                        <div className={styles.manaus}>
                            <div>
                                <p>Hotel+Aéreo</p>
                                <h1>Manaus</h1>
                            </div>
                            <h2>R$ 1.600</h2>
                            <button>Ver detalhes</button>
                        </div>
                    </div>
                </div>
            </section>
            <section className={styles.categoria}>
                <div>
                    <h1>Busque por categoria</h1>
                    <div className={styles.barra}></div>
                </div>
                <div>
                    <div className={styles.box}>
                        <img src="src/assets/pacotes-nacionais.png" alt=" "></img>
                        <h1>Pacotes nacionais</h1>
                    </div>
                    <div className={styles.box}>
                        <img src="src/assets/pacotes-internacionais.png" alt=" "></img>
                        <h1>Pacotes internacionais</h1>
                    </div>
                    <div className={styles.box}>
                        <img src="src/assets/hotel-hospedagem.png" alt=" "></img>
                        <h1>Hotéis e hospedagens</h1>
                    </div>
                    <div className={styles.box}>
                        <img src="src/assets/bagagem-extra.png" alt=" "></img>
                        <h1>Bagagem extra</h1>
                    </div>
                </div>
            </section>
            <section className={styles.destino}>
                <div>
                    <h1>Destinos populares</h1>
                    <div className={styles.barra}></div>
                </div>
                <div>
                    <div className={styles.espaco}>
                        <img src="src/assets/card-japa.jpeg"></img>
                        <h1>Tóquio</h1>
                        <p>Tóquio é uma cidade vibrante e cosmopolita, com seus templos históricos, museus de arte moderna e arranha-céus icônicos. Não perca a chance de mergulhar em sua atmosfera fascinante.</p>
                        <button>Saiba mais</button>
                    </div>
                    <div className={styles.espaco}>
                    <img src="src/assets/osaka.jpeg"></img>
                        <h1>Osaka</h1>
                        <p>Osaka é uma cidade agitada e moderna no Japãos. A cidade é famosa por sua gastronomia deliciosa e por ser um excelente ponto de partida para explorar outras cidades japonesas próximas.</p>
                        <button>Saiba mais</button>
                    </div>
                </div>
            </section>
            <section className={styles.pagamento}>
                <div>
                    <h1>Condições de pagamento</h1>
                    <div className={styles.barra}></div>
                </div>
                <div>
                    <div className={styles.text}>
                        <h1>Pagamento em até 12x!</h1>
                        <p>Viaje pagando em até 12 parcelas no cartão, à vista no crédito com 5% de desconto ou no Pix com 10% de desconto!</p>
                    </div>
                    <div className={styles.imagens}>
                        <div>
                            <img src="src/assets/montagem1.png"></img>
                            <img src="src/assets/montagem2.png"></img>
                        </div>
                        <img src="src/assets/montagem3.png"></img>
                    </div>
                </div>
            </section>
            <section className={styles.depoimento}>
                <div>
                    <h1>Depoimentos</h1>
                    <div className={styles.barra}></div>
                </div>
                <div>
                    <div className={styles.caixa}>
                        <p>A Jornada foi uma das melhores agências de viagens que eu já experimentei. O serviço ao cliente foi excepcional, e toda a equipe foi muito atenciosa e prestativa.</p>
                        <div>
                            <img src="src/assets/avatar-talita.png"></img>
                            <div>
                                <h1>Talita Villas Boas</h1>
                                <img src="src/assets/estrelas.png"></img>
                            </div>
                        </div>
                    </div>
                    <div className={styles.caixa}>
                        <p>Recomendo fortemente a agência de viagens Jornada. Eles oferecem um serviço personalizado e de alta qualidade que excedeu minhas expectativas em minha última viagem.</p>
                        <div>
                            <img src="src/assets/avatar-amari.png"></img>
                            <div>
                                <h1>Amari Salin</h1>
                                <img src="src/assets/estrelas.png"></img>
                            </div>
                        </div>
                    </div>
                    <div className={styles.caixa}>
                        <p>Minha viagem com a Jornada foi incrível! Recomendo muito a agência para quem busca uma experiência emocionante e personalizada.</p>
                        <div>
                            <img src="src/assets/avatar-lauro.png"></img>
                            <div>
                                <h1>Lauro B. Matos</h1>
                                <img src="src/assets/estrelas.png"></img>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <img src="src/assets/ultima.png"></img>
            </section>
        </div>
        </>
    )
}

