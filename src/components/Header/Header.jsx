
import { NavLink } from "react-router-dom";
import styles from "./header.module.css"
import React, { useState } from 'react';


function Header() {
    const [isOpen, setIsOpen] = useState(false);
    const [selectedOption, setSelectedOption] = useState(null);


    const toggleDropdown = () => {
        setIsOpen(!isOpen);
      };
   
    const handleOptionClick = () => {
        setIsOpen(false);
      };


    return (
        <div className={styles.header}>
            <section>
                <img src="src/assets/Logo-header.png" alt=" "></img>
            </section>
            <section className={styles.texto}>
                <NavLink to="/Blog"><h1 className={styles.h1}>Blog</h1></NavLink>
                <NavLink to="/Pacotes-viagens"><h1 className={styles.h1}>Pacotes de viagem</h1></NavLink>
                <div>
                    <img src="src/assets/bandeira-brasil.png" alt=" "></img>
                    <h1>Português</h1>
                </div>
                <NavLink to="/Contato"><h1 className={styles.h1}>Contato</h1></NavLink>
            </section>
            <div onClick={toggleDropdown} className={styles.menu}>
                <div></div>
                <div></div>
                <div></div>
            {isOpen && (
                <div className={styles.menu_lista}>
                    <ul>
                        <li onClick={ () => handleOptionClick}>Blog</li>
                        <li onClick={ () => handleOptionClick}>Pacotes de viagem</li>
                        <li> onClick={ () => handleOptionClick}Português</li>
                        <li onClick={ () => handleOptionClick}>Contato</li>
                    </ul>
                </div>
            )}
            </div>
        </div>
    )
}


export default Header






