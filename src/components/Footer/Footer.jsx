import styles from "./footer.module.css"


function Footer() {
    return (
        <div className={styles.footer}>
            <section>
                <img src="src/assets/Logo-footer.png" alt=" "></img>
                <div>
                    <p>Horário de atendimento: 08h - 20h (Segunda a Sábado)</p>
                    <p>Desenvolvido por Alura. Projeto fictício sem fins comerciais.</p>
                </div>
            </section>
            <section>
                <p>Acesse nossas redes:</p>
                <div>
                    <img src="src/assets/sapp.png"></img>
                    <img src="src/assets/insta.png"></img>
                    <img src="src/assets/twitter.png"></img>
                </div>
            </section>
        </div>
    )
}

export default Footer