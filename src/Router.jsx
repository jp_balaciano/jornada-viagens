import { Routes, Route } from "react-router-dom"
import { BaseLayout } from "./BaseLayout"
import { Home } from "./Home-page/Home"
import { Blog } from "./Blog/Blog"
import { Pacotes } from "./Pacotes-viagens/Pacotes"
import { Contato } from "./Contato/Contato"

export function Router() {
    return (
        <Routes>
            <Route path="/" element={<BaseLayout></BaseLayout>}>
                <Route path="/" element={<Home />}/>
                <Route path="/Blog" element={<Blog />}></Route>
                <Route path="/Pacotes-viagens" element={<Pacotes />}></Route>
                <Route path="/Contato" element={<Contato />}></Route>
            </Route>
        </Routes>
    )
}