import style from "./blog.module.css"

export function Blog() {
    return(
        <div className={style.blog}>
            <div>
                <h1>Blog</h1>
                <div className={style.barra}></div>
            </div>
            <section className={style.comentario}>
                <div className={style.card}>
                    <div>
                        <h2>João</h2>
                        <div>
                            <h3>Destino: Orlando</h3> 
                            <p>Nota: 10</p> 
                        </div>
                    </div>
                    
                    <p>O melhor lugar que eu já viajei foi para orlando, lá tudo é mágico e muito bem cuidado. Além de ser um espaço muito recomendado para crianças. O único lado negativo é o preço do dólar que está bem elevado comparado á outras épocas, porém você consegue encontrar preços muito bons lá, principalmente em eletrônicos.</p>
                </div>
                <div className={style.card}>
                    <div>
                        <h2>Pedro</h2>
                        <div>
                            <h3>Destino: Japão</h3> 
                            <p>Nota: 7</p> 
                        </div>
                        </div>
                        <p>O lugar é lindo, bem cuidado, com muitas árvores, cachoeiras, montanhar e parques. Porém a viagem é muito cansativa, porque é muito tempo de avião e a língua é uma dificuldade aburdamente grande, para se comunicar é bem difícil pois poucas pessoas falam um inglês de qualidade e quando falam, é complicado de entender por conta do sutaque</p>
                    </div>
            </section>
        </div>
    )
}

